# -*- coding: utf-8 -*-

from unittest.mock import sentinel, NonCallableMock
import pytest
import inspect

import pop.mods.pop.testing as testing
from pop.hub import Hub


class TestLazyPop:
    def test_hub_storage(self):
        hub = Hub()
        l_hub = testing._LazyPop(hub)

        assert l_hub._hub() is hub
        assert l_hub._lazy_hub() is l_hub
        assert l_hub._LazyPop__lut[hub] is l_hub

        assert l_hub.pop._hub() is hub

    def test_lazy(self):
        hub = Hub()
        hub.pop.sub.add("tests.mods")

        # pylint: disable=pointless-statement
        l_hub = testing.MockHub(hub)
        assert len(l_hub._LazyPop__lut) == 3
        l_hub.mods
        assert len(l_hub._LazyPop__lut) == 4
        l_hub.mods.testing
        assert len(l_hub._LazyPop__lut) == 5
        l_hub.mods.testing.echo
        assert len(l_hub._LazyPop__lut) == 6

    def test_reset(self):
        hub = Hub()
        hub.pop.sub.add("tests.mods")

        # pylint: disable=pointless-statement
        l_hub = testing.MockHub(hub)

        mtesting = l_hub.mods.testing
        echo = l_hub.mods.testing.echo
        noparam = l_hub.mods.testing.noparam
        assert len(l_hub._LazyPop__lut) == 7

        l_hub.mods.testing._reset()

        assert len(l_hub._LazyPop__lut) == 5
        assert l_hub.mods.testing is mtesting
        assert l_hub.mods.testing.echo is not echo
        assert l_hub.mods.testing.noparam is not noparam

    def test_recursive_reset(self):
        hub = Hub()
        hub.pop.sub.add("tests.mods")

        # pylint: disable=pointless-statement
        l_hub = testing.MockHub(hub)

        mtesting = l_hub.mods.testing
        mtest = l_hub.mods.test
        assert len(l_hub._LazyPop__lut) == 6
        l_hub.mods.testing.echo
        l_hub.mods.test.ping
        assert len(l_hub._LazyPop__lut) == 8

        l_hub.mods._reset()
        assert len(l_hub._LazyPop__lut) == 4
        assert l_hub.mods.testing is not mtesting
        assert l_hub.mods.test is not mtest

    def test_reset_assigment(self):
        hub = Hub()
        hub.pop.sub.add("tests.mods")

        # pylint: disable=pointless-statement
        l_hub = testing.MockHub(hub)

        aosnthu = object()
        l_hub.mods.aosnthu = aosnthu
        l_hub.mods._reset()

        assert not hasattr(l_hub.mods, "aosnthu")

    @pytest.mark.skip
    def test_reset_coupling(self):
        hub = Hub()

        obj = object()
        hub.obj = obj
        hub.pop.obj = obj

        l_hub = testing.MockHub(hub)
        l_obj1 = l_hub.obj
        assert l_hub.pop.obj is l_hub.obj

        l_hub.pop._reset()
        l_obj2 = l_hub.obj
        assert l_hub.pop.obj is l_hub.obj
        assert l_obj1 is not l_obj2

    def test_in_dict(self):
        # for autocompletion, all attributes should exist, even if it hasn't been called.
        hub = Hub()
        hub.pop.sub.add("tests.mods")

        l_hub = testing._LazyPop(hub)
        assert "mods" in l_hub.__dict__

    def test_duplicate_object(self):
        hub = Hub()
        hub.pop.sub.add("tests.mods")

        hub.test_val = sentinel.test_val
        hub.mods.test_val = sentinel.test_val
        hub.mods.testing.test_val = sentinel.test_val

        l_hub = testing._LazyPop(hub)

        assert isinstance(l_hub.test_val, NonCallableMock)
        assert l_hub.test_val is l_hub.mods.test_val
        assert l_hub.mods.test_val is l_hub.mods.testing.test_val

    def test_duplicate_hub(self):
        hub = Hub()
        hub.pop.sub.add("tests.mods")

        hub.hub = hub
        hub.mods.hub = hub
        hub.mods.foo.hub = hub

        l_hub = testing._LazyPop(hub)

        assert l_hub.hub is l_hub
        assert l_hub.mods.hub is l_hub
        assert l_hub.mods.foo.hub is l_hub

    def test_recursive_subs(self):
        hub = Hub()
        hub.pop.sub.add("tests.mods")
        hub.pop.sub.add("tests.mods.nest", sub=hub.mods)
        l_hub = testing._LazyPop(hub)

        assert hub.mods.nest.basic.ret_true()

        with pytest.raises(NotImplementedError):
            l_hub.mods.nest.basic.ret_true()

    def test_var_exists_enforcement(self):
        hub = Hub()
        hub.pop.sub.add("tests.mods")

        hub.FOO = "foo"
        hub.mods.FOO = "foo"
        hub.mods.testing.FOO = "foo"

        l_hub = testing._LazyPop(hub)

        for _ in (l_hub, l_hub.mods, l_hub.mods.testing):
            with pytest.raises(AttributeError):
                l_hub.BAZ

    def test_recursive_get(self):
        hub = Hub()
        hub.pop.sub.add("tests.mods")
        assert hub.mods
        l_hub = testing._LazyPop(hub)

        result = getattr(l_hub, "mods.foo")
        assert result is l_hub.mods.foo

    def test_find_subs(self):
        hub = Hub()
        hub.pop.sub.add("tests.mods")
        l_hub = testing._LazyPop(hub)

        subs = l_hub._find_subs()
        assert len(subs) == 2
        assert getattr(hub, subs[0][0]) is subs[0][1]

        # test nested subs
        hub.pop.sub.add(dyne_name="dn1")
        hub.pop.sub.load_subdirs(hub.dn1, recurse=True)
        l_hub = testing._LazyPop(hub)
        subs = l_hub._find_subs()
        assert len(subs) == 6
        assert len(subs[5][0].split(".")) == 4
        assert getattr(hub, subs[5][0]) is subs[5][1]

    def test_resolve_this(self):
        hub = Hub()
        hub.pop.sub.add("tests.mods")
        mock_hub = hub.pop.testing.mock_hub()
        from tests.mods.test import this, double_underscore

        assert mock_hub.mods.test.ping() is this(mock_hub)
        double_underscore(mock_hub)


class TestMockHub:
    hub = Hub()
    hub.pop.sub.add("tests.mods")
    mock_hub = hub.pop.testing.mock_hub()

    def test_mock_hub_dereference_errors(self):
        with pytest.raises(AttributeError, match="has no attribute 'nosub'"):
            self.mock_hub.nosub.nomodule.nofunc()

        with pytest.raises(AttributeError, match="has no attribute 'nomodule'"):
            self.mock_hub.mods.nomodule.nofunc()

        with pytest.raises(AttributeError, match="has no attribute 'nofunc'"):
            self.mock_hub.mods.testing.nofunc()

    def test_mock_hub_function_enforcement(self):
        with pytest.raises(TypeError, match="missing a required argument: 'param'"):
            self.mock_hub.mods.testing.echo()

    def test_mock_hub_return_value(self):
        self.mock_hub.mods.testing.echo.return_value = sentinel.myreturn
        assert self.mock_hub.mods.testing.echo("param") is sentinel.myreturn

    @pytest.mark.asyncio
    async def test_async_echo(self):
        val = "foo"
        assert await self.hub.mods.testing.async_echo(val) == val

        self.mock_hub.mods.testing.async_echo.return_value = val
        assert await self.mock_hub.mods.testing.async_echo(val + "change") == val

    def test_signature(self):
        assert inspect.signature(self.hub.pop.sub.add) == inspect.signature(
            self.mock_hub.pop.sub.add
        )


class TestNoContractHub:
    hub = Hub()
    hub.pop.sub.add("tests.mods")
    nocontract_hub = hub.pop.testing.fn_hub()

    def test_call(self):
        val = self.nocontract_hub.mods.testing.echo(sentinel.param)
        assert val is sentinel.param

    def test_signature(self):
        assert inspect.signature(self.hub.pop.sub.add) == inspect.signature(
            self.nocontract_hub.pop.sub.add
        )


class TestMockContracted:
    hub = Hub()
    hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")

    def test_hub_contract(self):
        assert self.hub.mods.testing.echo("foo") == "contract foo"

    def test_contract_hub_contract(self):
        m_echo = testing.mock_contracted(None, self.hub.mods.testing.echo)
        m_echo.func.return_value = "bar"
        assert m_echo("foo") == "contract bar"

    def test_contract_hub_getattr(self):
        assert testing.mock_contracted(None, self.hub.mods.testing.echo).return_value

    def test_contract_hub_module(self):
        m_echo = testing.mock_contracted(None, self.hub.mods.testing.echo)
        func_module = self.hub.mods.testing.echo.func.__module__
        assert m_echo.func.__module__ == func_module

    def test_signature(self):
        m_sig = testing.mock_contracted(None, self.hub.mods.testing.signature_func)
        assert str(m_sig.signature) == "(hub, param1, param2='default')"

    def test_get_arguments(self):
        m_sig = testing.mock_contracted(None, self.hub.mods.testing.signature_func)
        m_sig("passed in")

    def test_copy_func_attributes(self):
        echo = testing.mock_contracted(None, self.hub.mods.testing.echo)
        attr_func = testing.mock_contracted(None, self.hub.mods.testing.attr_func)

        with pytest.raises(AttributeError):
            assert echo.func.test
        assert attr_func.func.test is True

        with pytest.raises(AttributeError):
            assert echo.func.__test__
        assert attr_func.func.__test__ is True


class TestContractHub:
    hub = Hub()
    hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")
    contract_hub = testing.ContractHub(hub)

    def test_hub_contract(self):
        assert self.hub.mods.testing.echo("foo") == "contract foo"

    def test_contract_hub_contract(self):
        assert isinstance(self.contract_hub.mods.testing.echo, testing.Contracted)

    @pytest.mark.asyncio
    async def test_async_echo(self):
        val = "foo"
        expected = "async contract " + val
        assert await self.hub.mods.testing.async_echo(val) == expected

        self.contract_hub.mods.testing.async_echo.func.return_value = val
        assert (
            await self.contract_hub.mods.testing.async_echo(val + "change") == expected
        )

    def test_contract_hub_inspect(self):
        # demo ways that we can inspect the contract system
        assert len(self.contract_hub.mods.testing.echo.contracts) == 1
        assert "call_signature_func" in dir(
            self.contract_hub.mods.testing.echo.contracts[0]
        )

    def test_contract_hub_modify(self):
        contract_hub = testing.ContractHub(self.hub)

        # modifying the contract
        contract_hub.mods.testing.echo.func.return_value = "bar"
        assert contract_hub.mods.testing.echo("foo") == "contract bar"
        contract_hub.mods.testing.echo.contract_functions["call"] = []
        assert contract_hub.mods.testing.echo("foo") == "bar"

        # verify that modification didn't mess with the real hub:
        assert self.hub.mods.testing.echo("foo") == "contract foo"


class TestMockAttrHub:
    hub = Hub()
    hub.pop.sub.add("tests.mods", contracts_pypath="tests.contracts")
    mock_attr_hub = testing.MockAttrHub(hub)

    def test_hub_contract(self):
        assert self.hub.mods.testing.echo("foo") == "contract foo"
        assert self.mock_attr_hub.mods.testing.echo("foo") == "contract foo"

        self.hub.VAL = {}
        assert "Mock" in str(self.mock_attr_hub.OPT)


class TestHybridHub:
    """
    We override assignment so that if you take two types of test hubs, you can assign freely between them:

      mock_hub.sub.mod = fn_hub.sub.mod

    This will create a hybrid test hub.

    Everything below mock_hub.sub.mod will be generated as fn_hub, everything else using mock_hub.
        """

    hub = Hub()

    def test_contracted_assignment(self):
        m_hub = self.hub.pop.testing.mock_hub()
        fn_hub = self.hub.pop.testing.fn_hub()
        m_hub.pop.sub.add = fn_hub.pop.sub.add

        assert m_hub.pop.sub.add.hub is m_hub

    def test_mod_assignment(self):
        m_hub = self.hub.pop.testing.mock_hub()
        fn_hub = self.hub.pop.testing.fn_hub()
        m_hub.pop.testing = fn_hub.pop.testing

        assert m_hub.pop.testing.__class__ is fn_hub.__class__
        assert m_hub.pop.testing.fn_hub.hub is m_hub

    def test_sub_assignment(self):
        m_hub = self.hub.pop.testing.mock_hub()
        fn_hub = self.hub.pop.testing.fn_hub()
        m_hub.pop = fn_hub.pop

        assert m_hub.pop._lazy_hub() is m_hub
        assert m_hub.pop.__class__ is fn_hub.pop.__class__
        assert m_hub.pop.testing._lazy_hub() is m_hub
        assert m_hub.pop.testing.__class__ is m_hub.pop.__class__

    def test_real_hub_assignment(self):
        m_hub = self.hub.pop.testing.mock_hub()

        # contracted works
        m_hub.pop.sub.add = self.hub.pop.sub.add
        assert m_hub.pop.sub.add.hub is m_hub

        match = (
            "Mixing of real and test hubs is not supported. "
            + "Contracteds \\(hub.sub.mod.func\\) are supported."
        )
        with pytest.raises(TypeError, match=match):
            m_hub.pop.sub = self.hub.pop.sub

        with pytest.raises(TypeError, match=match):
            m_hub.pop = self.hub.pop
