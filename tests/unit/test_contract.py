from pop.contract import Contracted
import inspect


def test_contracted_shortcut():
    def f(hub):
        pass

    c = Contracted(hub="a hub", contracts=[], func=f, ref=None, name=None)
    c.contract_functions["pre"] = [
        None
    ]  # add some garbage so we raise if we try to evaluate contracts

    c()


def test_contracted_inspect():
    def f(hub, p1, p2=None):
        pass

    c = Contracted(None, None, f, None, None)

    assert str(inspect.signature(c)) == str(inspect.signature(f))
    assert str(inspect.signature(c)) == "(hub, p1, p2=None)"
